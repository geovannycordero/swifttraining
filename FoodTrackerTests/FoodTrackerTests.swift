//
//  FoodTrackerTests.swift
//  FoodTrackerTests
//
//  Created by Geovanny Cordero on 2/17/20.
//  Copyright © 2020 Geovanny Cordero. All rights reserved.
//

import XCTest
@testable import FoodTracker

class FoodTrackerTests: XCTestCase {
    
    //MARK: Meal class tests
    
    // Confirm that the Meal initializer returns a Meal object when passes a valid parameters
    func testMealInitializationSucceds () {
        let zeroRatingMeal = Meal.init(name: "zero", image: nil, rating: 0)
        XCTAssertNotNil(zeroRatingMeal)

        // Highest positive rating
        let positiveRatingMeal = Meal.init(name: "positive", image: nil, rating: 5)
        XCTAssertNotNil(positiveRatingMeal)
    }
    
    // Confirm that the Meal initializer return nill when passed a negative rating or an empty string
    func testMealInitializationFails () {
        let negativeRatingMeal = Meal.init(name: "negative", image: nil, rating: -1)
        XCTAssertNil(negativeRatingMeal)

        let largeRatingMeal = Meal.init(name: "large", image: nil, rating: 6)
        XCTAssertNil(largeRatingMeal)

        let emptyStringMeal = Meal.init(name: "", image: nil, rating: 0)
        XCTAssertNil(emptyStringMeal)
    }
}
