# Swift Training - FoodTracker App

Source: [Start Developing iOS Apps (Swift)](https://developer.apple.com/library/archive/referencelibrary/GettingStarted/DevelopiOSAppsSwift/index.html#//apple_ref/doc/uid/TP40015214-CH2-SW1)

Starting point to learn how to create applications that run on iPhone and iPad. Set of incremental lessons as a guided introduction to create a first application, including tools, main concepts and best practices.

In these lessons, I will be building a simple meal-tracking app called FoodTracker. This app shows a list of meals, including a meal name, rating, and photo. A user can add, remove, or edit a meal. To add a new meal or edit an existing one, users navigate to a different screen where they can specify a name, rating, and photo for a particular meal.

## Concepts covered
- iOS app development
- [Swift programming language](https://swift.org)
- Features of Xcode, Apple’s IDE

## Requisites
1. Mac Computer (macOS 10.11.5 or later)
2. Latest version of Xcode (Xcode includes all the features you need to design, develop, and debug an app)
